# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
#Game.destroy_all


# Games
20.times do |i|
    Game.create(
        name: Faker::Lorem.sentence(3),
        description: Faker::Lorem.paragraph,
        price: (rand(10.5..200.5)).round(2),
        publication: Time.now
        )
    end
    
# Categories
20.times do |i|
    Category.create(name: Faker::Lorem.sentence(2))
end

# Regions
20.times do |i|
    Region.create(name: Faker::Lorem.sentence(2))
end

# Users
20.times do |i|
    User.create(
        name: Faker::Name.name,
        email: Faker::Internet.free_email,
        region_id: 1,
        encrypted_password: Faker::Internet.password
        )
end

# Game Categories
5.times do |j|
    10.times do |i|
        GamesCategory.create(game_id: i+1, category_id: j+1)
    end
end

# My Games
10.times do |j|
    10.times do |i|
        MyGame.create(user_id: j+1, game_id: i+1)
    end
end