class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :name
      t.text :description
      t.string :picture
      t.decimal :price
      t.datetime :publication

      t.timestamps
    end
  end
end
