class CreateArchives < ActiveRecord::Migration[5.2]
  def change
    create_table :archives do |t|
      t.string :name
      t.string :attachment
      t.references :game, foreign_key: true

      t.timestamps
    end
  end
end
