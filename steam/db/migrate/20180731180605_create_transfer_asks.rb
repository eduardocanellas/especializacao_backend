class CreateTransferAsks < ActiveRecord::Migration[5.2]
  def change
    create_table :transfer_asks do |t|
      t.integer :user_id
      t.integer :friend_id
      t.integer :my_game_id
      t.boolean :status

      t.timestamps

    end
    add_index "transfer_asks", ["user_id"], name: "index_transfer_asks_on_user_id", using: :btree
    add_index "transfer_asks", ["friend_id"], name: "index_transfer_asks_on_friend_id", using: :btree
    add_index "transfer_asks", ["my_game_id"], name: "index_transfer_asks_on_my_game_id", using: :btree
  end
end
