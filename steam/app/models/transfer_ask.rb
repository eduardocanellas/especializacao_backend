class TransferAsk < ApplicationRecord
    belongs_to :user
    belongs_to :friend, class_name: "User"
    belongs_to :my_game
end