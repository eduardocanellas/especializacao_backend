class User < ApplicationRecord
  belongs_to :region, required: false
  has_many :my_games
  has_many :transfer_asks
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

end
