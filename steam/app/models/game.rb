class Game < ApplicationRecord
    has_many :game_categories
    has_many :my_games
    has_many :archives

    mount_uploader :picture, ImageUploader
end
