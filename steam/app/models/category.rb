class Category < ApplicationRecord
    has_many :games
    has_many :game_categories
end
