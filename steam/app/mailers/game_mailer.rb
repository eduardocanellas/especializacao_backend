class GameMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.game_mailer.new_game.subject
  #
  def new_game(game)
    @greeting = "Ola, tudo bom?"
    @user = User.first
    @game = game
    
    mail to: @user.email, subject: "Novo jogo"
  end
end
