class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@astolfo.com'
  layout 'mailer'
end
