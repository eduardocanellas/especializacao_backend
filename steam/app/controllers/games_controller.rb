class GamesController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:addCategory]
  before_action :set_game, only: [:show, :edit, :update, :destroy]
  
  # GET /games
  # GET /games.json
  def index
    @games = Game.all.paginate(page: params[:page], per_page: 10)
  end

  # GET /games/1
  # GET /games/1.json
  def show
    # GameMailer.new_game(@game).deliver_now
    
    tmp = GamesCategory.where('game_id = '+params[:id].to_s)
    @gameCategories = []

    tmp.each do |gameCategory|
      category = Category.find_by(id: gameCategory.category_id)
      @gameCategories.append(category)
    end

    @categories = Category.all
    @newGameCategory = GamesCategory.new
  end

  def addCategory
    idGame = params[:newCategory][:game_id]
    idCategory = params[:newCategory][:category_id]

    newCategory = GamesCategory.new(game_id: idGame, category_id: idCategory)

    if !GamesCategory.find_by(category_id: idCategory, game_id: idGame)
      newCategory.save
    else
      flash.notice = 'Essa Categoria já existe!'
    end

    redirect_to '/games/'+idGame.to_s
  end

  def removeCategory
    idGame = params[:id]
    idCategory = params[:category_id]

    category = GamesCategory.find_by(game_id: idGame, category_id: idCategory)
    category.destroy

    flash.notice = "Categoria removida com sucesso!"
    redirect_to '/games/'+params[:id].to_s
  end

  # GET /games/new
  def new
    @game = Game.new
    @game.picture = params[:picture]
  end

  # GET /games/1/edit
  def edit
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.new(game_params)

    respond_to do |format|
      if @game.save
        format.html { redirect_to @game, notice: 'Game was successfully created.' }
        format.json { render :show, status: :created, location: @game }
      else
        format.html { render :new }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_to do |format|
      if @game.update(game_params)
        format.html { redirect_to @game, notice: 'Game was successfully updated.' }
        format.json { render :show, status: :ok, location: @game }
      else
        format.html { render :edit }
        format.json { render json: @game.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    @game.destroy
    respond_to do |format|
      format.html { redirect_to games_url, notice: 'Game was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:name, :description, :picture, :price, :publication)
    end
end
