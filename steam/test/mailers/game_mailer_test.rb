require 'test_helper'

class GameMailerTest < ActionMailer::TestCase
  test "new_game" do
    mail = GameMailer.new_game
    assert_equal "New game", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
