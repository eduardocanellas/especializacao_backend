Rails.application.routes.draw do
  devise_for :users, path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret'}

  root 'users#index'
  resources :users, except: [:new]
  resources :games
  post '/games/addCategory', to: 'games#addCategory'
  delete 'games/:id/:category_id' => 'games#removeCategory'
  resources :categories
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
